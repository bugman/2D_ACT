# 2D_ACT
邮箱:1007991486@qq.com
#### 介绍
2D横版格斗
<br>
![曲线位移](https://images.gitee.com/uploads/images/2019/0525/190437_0a5d68c4_952685.gif "Honeycam 2019-05-25 13-11-17.gif")
![技能同步](https://images.gitee.com/uploads/images/2019/0518/180018_1493e100_952685.gif "Honeycam 2019-05-18 17-59-37.gif")
![子弹](https://images.gitee.com/uploads/images/2019/0518/175220_3b438faa_952685.gif "Honeycam 2019-05-18 17-50-14.gif")
![移动同步](https://images.gitee.com/uploads/images/2019/0301/120012_4b830ae9_952685.gif "Honeycam 2019-03-01 11-59-08.gif")

<br> **2019.5.25：** 
<br> 玩家高度放入Box3D中的centerZ变量中
<br> 规范了Box3D中的命名规范,方便后续写逻辑
<br> 角色左右移动为X轴,上下移动为Y轴,跳跃为Z轴;玩家碰撞器也有3个分量,组成长方体,length对应X，width对应Y，height对应Z
<br> 加入了曲线移动状态,贝塞尔曲线实现

<br> **2019.3.8：** 
<br> **技能和伤害包同步** 
<br> 客户A释放技能，给服务器同步技能信息，服务器进行广播(A主动释放技能,如果等待服务器回包,可能不够流畅)
<br> 客户端B收到技能信息，播放技能
<br> 伤害检测由A检测，有攻击到实体给服务器同步，服务器再广播
<br> 客户端B收到伤害包，单纯做伤害包的动画,音效,位移,扣血...表现


<br> **2019.3.1：** 
<br> 服务器实体与客户端实体用同一套代码
<br> 客户端实体有展示，展示组件使用RoleShowWidget,客户端的表现都写在这个脚本中
<br> **关于移动同步**
<br> 客户端A位置同步客户端B流程
<br> 客户端A每帧移动同步给服务器，服务器转发给其他客户端
<br> 服务器不做碰撞验证，A客户端给服务器同步位置的时候会做碰撞验证
<br> 服务器同步给B客户端同步A客户端位置，B客户端也不做碰撞验证

<br> **2019.2.27：** 
<br> **物理系统** 
<br> 游戏中的物理不再与Unity耦合,客户端和服务器共用一套物理系统
<br> 2D的碰撞全部都是垂直矩形,接口isCollision()检测是否与场景静态物体碰撞
<br> 效率问题,10W次调用开销20ms,客户端每次移动检测与静态物体碰撞,开销不大
<br>          服务器开销有点大,服务器只做位置碰撞检测,攻击伤害检测由客户端做

<br> **2019.2.23：** 
<br> 1.制作一个类似DNF的2D游戏(Github相关工具https://github.com/tianjiuwan)
<br> ** 2.资源相关：** 
<br>  美术：Assets/Res/Arts下面存放美术资源和策划excel配置，可以有冗余资源(不会打包到ab中)
<br>               Assets/Res/AssetBundle下面的存放的美术资源会打包成ab
<br>               角色模型动画使用spine制作
<br>          程序：使用Tools下面的BuildAsset可以打包资源的assetbundle
<br>  **3.代码相关：** 
<br>          ** 资源管理** ：位于UnityPool命名空间下，AssetBundle的管理使用引用计数，对象池时间管理，关卡管理
<br>                   PoolMgr.Instance.getObj()获取一个GameObject,Sprite,异步加载所有依赖
<br>                   PoolMgr.Instance.recyleObj()回收一个GameObject
<br>                   PoolMgr.Instance.unLoad()中断加载
<br>                   PoolMgr.Instance.preLoad()预加载
<br>          ** C#对象池** ：位于Pool命名空间下，管理一些创建频繁，生命周期短的对象
<br>                   PoolMgr.Instance.getData<T>()泛型获取一个对象，对象必须继承PoolObject
<br>                   PoolMgr.Instance.recyleData()回收一个对象，对象必须继承PoolObject
<br>           **实体类** ：BaseEntity抽象出公有成员，管理实体模型加载状态，持有状态机，动画控制器..
<br>                 MainPlayer继承自BaseEntity,注册了输入监听
<br>                 EntityMgr.Instance.createRole<T>(roleData)创建一个实体，实体由管理器管理
<br>           **状态机** ：使用有限状态机管理实体状态
<br>                 目前做了一些简单的状态idle,run,jump,hit..
<br>           **战斗** ： 状态同步，由逻辑帧驱动，1秒30帧
<br>                动画，音效，特效..都有逻辑帧驱动
<br>           **网络** ：服务器使用DotNetty,ProtoBuf做数据传输协议(github有导出C# protobuf文件工具)
<br>                客户端维护Socket，可在C#序列化和反序列化协议，也可以在Lua那边序列化和反序列化协议
<br>           **Lua** ：使用tolua写热更新业务逻辑(github有导出lua protobuf文件工具)



                   
                   
                   
                     
                   
 