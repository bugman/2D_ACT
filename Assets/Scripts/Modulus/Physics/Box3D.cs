﻿namespace TwlPhy
{
    using System;

    /// <summary>
    /// 3D长方体 坐标有高度z  大小有高度h
    /// </summary>
    public class Box3D : Box2D
    {
        public float centerZ
        {
            set;
            get;
        }
        public float height
        {
            protected set;
            get;
        }

        public Box3D(float x, float y, float l, float w) : base(x, y, l, w)
        {

        }

        public Box3D(float x, float y, float z, float l, float w, float h) : base(x, y, l, w)
        {
            centerZ = z;
            height = h;
        }

        //BOX的锚点在中心
        public void updatePosHeight(float posZ)
        {
            centerZ = posZ;
            maxH = centerZ + height / 2;
            minH = centerZ - height / 2;
        }

        protected float minH;
        public float MinH
        {
            get
            {
                return minH;
            }
        }

        protected float maxH;
        public float MaxH
        {
            get
            {
                return maxH;
            }
        }

    }
}
