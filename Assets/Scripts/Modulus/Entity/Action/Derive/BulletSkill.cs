﻿
namespace TwlAction
{
    using System;
    using System.Collections.Generic;
    using TwlPhy;
    using Config;

    /// <summary>
    /// 子弹类型技能
    /// 显示载体 模型
    /// 碰撞检测 TwlPhy
    /// 需要每帧更新
    /// </summary>
    public class BulletSkill : BaseAction
    {
        public SkillDisplayWidget skillDisplay;//客户端有展示
        private UnityEngine.GameObject unityObj = null;
        public BulletConfig bulletConfig;
        private Vector2 dir = Vector2.zero;

        public override string resName
        {
            get
            {
                return bulletConfig.resName;
            }
        }

        //逻辑帧tick
        public override void Tick()
        {
            if (FrameMgr.NowFrame - this.spawnFrame >= bulletConfig.liveFrame)
            {
                this.onDeath();
            }
            else
            {
                boxMove(bulletConfig.flySpeed);
                updateDisplay();
            }
        }

        void boxMove(float flySpeed)
        {
            //进行碰撞检测
            float posX = flySpeed;
            //子弹连续检测碰撞 x轴会被拉长 其他2个轴不变
            float sizeX = bulletConfig.size[0];
            bool isLarge = posX > sizeX;            
            float bx = isLarge ? box3d.centerX + sizeX/2 + posX / 2*dir.x : box3d.centerX;
            float blen = isLarge ? posX  : sizeX;
            DymicBox3D box = new DymicBox3D(bx, box3d.centerY, box3d.centerZ, blen, bulletConfig.size[1], bulletConfig.size[2]);
            List<long> lst = HartUtils.checkHit(this.actionData.ownerId, box);
            this.box3d.AddCenter(dir.x * flySpeed, dir.y * flySpeed);
            if (lst != null && lst.Count > 0)
            {
                //Logger.logError("bullet hit " + lst[0]);
                //单机
                doHit(lst[0]);
                //同步
                //SyncMoveUtils.syncHit(this.skillData.ownerId, lst, 1002, 1001);
                onDeath();
            }
        }

        void doHit(long roleId)
        {
            BaseEntity role = EntityMgr.getRole<BaseEntity>(roleId);
            if (role != null)
            {
                //击退
                //FSMArgs args = Pool.PoolMgr.Instance.getData<FSMArgs>();
                //args.cfgId = 1001;
                //BaseEntity agent = roleAgent;
                //args.data = new TwlPhy.Vector3(agent.DyBox.centerX, agent.DyBox.centerY, agent.DyBox.MaxH);
                //role.transFsm(FSM_Flag.Hit, args, true);
                //hp todo

                //浮空
                //FSMArgs kargs = new FSMArgs();
                //kargs.cfgId = 16;
                //role.transFsm(FSM_Flag.AirHit, kargs, true);

                //曲线浮空
                //FSMArgs cargs = new FSMArgs();
                //role.transFsm(FSM_Flag.CurveHit, null, true);

                ActionData data = new ActionData();
                data.actionId = bulletConfig.actionId;
                data.ownerId = roleId;
                data.actionGuid = MathUtils.UniqueID;
                ActionMgr.Instance.create<BaseAction>(data);
            }
        }

        protected override void createDisplay()
        {
            //Unity相关物件挂载点
            if (unityObj == null)
            {
                unityObj = new UnityEngine.GameObject(actionData.actionGuid.ToString());
            }
            //模型组件
            if (skillDisplay == null)
            {
                skillDisplay = unityObj.AddComponent<SkillDisplayWidget>();
                skillDisplay.setSkill(this);
            }
        }

        void updateDisplay()
        {
            if (this.skillDisplay != null)
            {
                this.skillDisplay.updateRenderOrder();
            }
        }

        public override void onSpawn()
        {
            BaseEntity role = roleAgent;
            if (role != null)
            {
                dir = role.lookFlag == LookFlag.Left ? Vector2.left : Vector2.right;
                float x = role.DyBox.centerX + 1f * dir.x;
                //y应该在枪口
                float y = role.DyBox.centerY;
                float z = role.DyBox.centerZ + role.HitBox.height * 0.4f;
                //float h = role.DyBox.centerZ + role.RoleData.hitBox.y / 2;
                box3d = new DymicBox3D(x, y, z, bulletConfig.size[0], bulletConfig.size[1], bulletConfig.size[2]);
                boxMove(0);
            }
        }

        protected override void readActionConfig()
        {
            bulletConfig = BulletConfig.get(actionData.actionId);
        }

        public override void onDeath()
        {
            base.onDeath();
        }

        public override void onDispose()
        {
            base.onDispose();
            if (skillDisplay != null)
            {
                skillDisplay.onDispose();
                skillDisplay = null;
            }
        }


    }
}
