﻿namespace TwlAction
{
    using System;
    using System.Collections.Generic;
    using Config;

    public class DamageBulletCaster : DamageBaseCaster
    {
        public override void castAction(ActionData actionData, AttackConfig attackConfig)
        {
            ActionData data = new ActionData();
            data.ownerId = actionData.ownerId;
            data.actionId = attackConfig.cfgId;
            data.actionGuid = MathUtils.UniqueID;
            data.ownerPos = actionData.ownerPos;
            ActionMgr.Instance.create<BulletSkill>(data);
        }
    }
}
