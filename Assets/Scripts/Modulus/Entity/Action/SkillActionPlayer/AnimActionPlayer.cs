﻿namespace TwlAction
{
    using System;
    using System.Collections.Generic;
    using Config;

    public class AnimActionPlayer : BaseActionPlayer
    {
        public override void initialize()
        {
            
        }

        public override void playAction(ActionData actionData, int cfgId)
        {
            AnimConfig animCfg = AnimConfig.get(cfgId);
            if (animCfg != null) {
                BaseEntity role = EntityMgr.getRole<BaseEntity>(actionData.ownerId);
                if (role != null) {
                    role.playAnim(animCfg.animName,animCfg.isLoop,animCfg.backAnim,true);
                }
            }
        }
    }
}
