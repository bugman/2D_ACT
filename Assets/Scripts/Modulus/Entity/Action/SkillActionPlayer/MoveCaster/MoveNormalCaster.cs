﻿namespace TwlAction
{
    using System;
    using System.Collections.Generic;
    using Config;
    using TwlPhy;

    /// <summary>
    /// 受击位移
    /// </summary>
    public class MoveNormalCaster : MoveBaseCaster
    {
        public override void castAction(ActionData actionData, MoveConfig moveConfig)
        {
            BaseEntity role = EntityMgr.getRole<BaseEntity>(actionData.ownerId);
            if (role != null) {
                FSMArgs args = new FSMArgs();
                FSM_Flag flag = role.Flag;
                if (flag == FSM_Flag.AirHit || flag == FSM_Flag.AirFall)
                {
                    args.cfgId = moveConfig.airForce;
                    role.transFsm(FSM_Flag.AirHit, args, true);
                }
                else {
                    args.cfgId = moveConfig.id;
                    args.data = new Vector3(-3, 0, 0);//应该拿攻击者位置 todo
                    role.transFsm(FSM_Flag.StandHit, args, true);
                }                
            }
        }



    }
}
