﻿namespace TwlAction
{
    using System;
    using System.Collections.Generic;
    using Config;

    /*  
     *  1 Normal
     *  2 Bullet
     */

    /// <summary>
    /// 伤害播放器
    /// 隐射到AttackConfig包含了攻击类型;攻击参数...
    /// 每次攻击行为 分发给相关执行者执行
    /// 1 普通攻击 读取AttackConfig的攻击参数 进行攻击
    /// 2 子弹攻击 子弹执行者 读取对应子弹配置 进行攻击
    /// 3 陷阱 读取陷阱表 todo 执行者自己实现
    /// 4 BUFF 读取buff表 todo 执行者自己实现
    /// </summary>
    public class DamageActionPlayer : BaseActionPlayer
    {
        private Dictionary<int, DamageBaseCaster> casterPool = null;

        public override void initialize()
        {
            casterPool = new Dictionary<int, DamageBaseCaster>();

            casterPool.Add(1,new DamageNormalCaster());
            casterPool.Add(2, new DamageBulletCaster());
        }

        public override void playAction(ActionData actionData, int cfgId)
        {
            AttackConfig attackConfig = AttackConfig.get(cfgId);
            if (attackConfig != null) {
                DamageBaseCaster caster;
                if (casterPool.TryGetValue(attackConfig.attckType, out caster)) {
                    caster.castAction(actionData,attackConfig);
                }
            }
        }


    }
}
