﻿using TwlPhy;
using TwlAction;
using UnityEngine;

public class MainPlayer : DynamicEntity
{
    public override TwlPhy.Vector2 moveDir
    {
        get
        {
            return this.dir;
        }

        set
        {
            this.dir = value;
            SyncMoveUtils.syncMove(this.UID);
            //if (this.dir!=value)
            //{                
            //    SyncMoveUtils.syncMove(this.UID);
            //}
        }
    }

    protected override void onStart()
    {
        initInput();
        initCamera();
    }

    void initInput()
    {
        InputMgr.Instance.addListener(onKeyDown);
        InputMgr.Instance.addUpListener(onKeyUp);
    }

    void initCamera()
    {

    }

    protected override void initFsm()
    {
        base.initFsm();
        //主角特有的状态
        fsm.addState(FSM_Flag.Run, new MainPlayerRunState());
        fsm.addState(FSM_Flag.Shoot, new ShootState());
    }

    void onKeyUp(KeyCode key)
    {
        if (moveKey == key)
        {
            this.moveDir = TwlPhy.Vector2.zero;
            moveKey = KeyCode.None;
            transFsm(FSM_Flag.Idle);
        }
    }

    public UnityEngine.KeyCode moveKey = UnityEngine.KeyCode.None;

    void onKeyDown(KeyCode key)
    {
        switch (key)
        {
            case KeyCode.LeftArrow:
                this.moveDir = TwlPhy.Vector2.left;
                moveKey = key;
                changeLookFlag(LookFlag.Left);
                doRun();
                break;
            case KeyCode.RightArrow:
                this.moveDir = TwlPhy.Vector2.left * -1;
                moveKey = key;
                changeLookFlag(LookFlag.Right);
                doRun();
                break;
            case KeyCode.UpArrow:
                this.moveDir = TwlPhy.Vector2.up;
                moveKey = key;
                doRun();
                break;
            case KeyCode.DownArrow:
                this.moveDir = TwlPhy.Vector2.up * -1;
                moveKey = key;
                doRun();
                break;
            case KeyCode.Q:
                break;
            case KeyCode.R:
                //int skillId = 1001;
                //FSMArgs args = Pool.PoolMgr.Instance.getData<FSMArgs>();
                //args.data = SkillConfigHelper.Instance.getConfig(skillId);
                //transFsm(FSM_Flag.Skill, args);
                //SyncMoveUtils.syncSkill(this.UID, skillId);
                break;
            case KeyCode.C:
                transFsm(FSM_Flag.Jump);
                break;
            case KeyCode.T:
                playAnim("jump", false, "idle");
                break;
            case KeyCode.H:
                FSMArgs hargs = new FSMArgs();
                hargs.cfgId = 60;
                transFsm(FSM_Flag.AirHit, hargs,true);
                break;
            case KeyCode.K:
                FSMArgs kargs = new FSMArgs();
                kargs.cfgId = 30;
                transFsm(FSM_Flag.AirHit, kargs, true);
                break;
            case KeyCode.L:
                FSMArgs largs = new FSMArgs();
                largs.cfgId = 15;
                transFsm(FSM_Flag.AirHit, largs, true);
                break;
            case KeyCode.G:
                ActionData gactionData = new ActionData();
                gactionData.actionId = 100002;
                gactionData.actionGuid = MathUtils.UniqueID;//应该服务器推送
                gactionData.ownerId = this.UID;
                ActionMgr.Instance.create<BaseAction>(gactionData);
                break;
            case KeyCode.F:
                //int fskillId = 1002;
                //FSMArgs fargs = Pool.PoolMgr.Instance.getData<FSMArgs>();
                //fargs.data = SkillConfigHelper.Instance.getConfig(fskillId);
                //transFsm(FSM_Flag.Skill, fargs);
                //SyncMoveUtils.syncSkill(this.UID, fskillId);

                ActionData actionData = new ActionData();
                actionData.actionId = 100001;
                actionData.actionGuid = MathUtils.UniqueID;//应该服务器推送
                actionData.ownerId = this.UID;
                ActionMgr.Instance.create<BaseAction>(actionData);
                break;
        }
    }

}
