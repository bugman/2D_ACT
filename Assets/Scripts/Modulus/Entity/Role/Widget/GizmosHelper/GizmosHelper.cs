﻿using System;
using UnityEngine;
using System.Collections.Generic;
using TwlPhy;

public class GizmosHelper
{

    public static void drawBox(DymicBox3D box)
    {
        GameObject go = new GameObject();
        GameObject.Destroy(go, 0.6f);
        go.AddComponent<GizmosWidget>().box = box;
    }

}

