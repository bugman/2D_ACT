﻿using System;
using System.Collections.Generic;
using UnityEngine;
using TwlPhy;

public class GizmosWidget : MonoBehaviour
{
    public DymicBox3D box;
    public Color color = Color.red;

    private void OnDrawGizmos()
    {
        if (box != null)
        {
            //float h = box.MaxH;
            //UnityEngine.Vector3 A = new UnityEngine.Vector3(box.Left, box.centerY + box.height / 2+h);
            //UnityEngine.Vector3 B = new UnityEngine.Vector3(box.Right, box.centerY + box.height / 2+h);
            //UnityEngine.Vector3 C = new UnityEngine.Vector3(box.Left, box.centerY - box.height / 2+h);
            //UnityEngine.Vector3 D = new UnityEngine.Vector3(box.Right, box.centerY - box.height / 2+h);
            //Gizmos.color = color;
            //Gizmos.DrawLine(A, B);
            //Gizmos.DrawLine(B, C);
            //Gizmos.DrawLine(C, D);
            //Gizmos.DrawLine(D, A);
            ////Gizmos.color = Color.cyan;
            UnityEngine.Vector3 top1 = new UnityEngine.Vector3(box.Left, box.MaxH + box.centerY);
            UnityEngine.Vector3 bottom1 = new UnityEngine.Vector3(box.Right, box.MinH + box.centerY);
            UnityEngine.Vector3 left1 = new UnityEngine.Vector3(box.Left, box.MinH + box.centerY);
            UnityEngine.Vector3 right1 = new UnityEngine.Vector3(box.Right, box.MaxH + box.centerY);
            Gizmos.color = Color.green;
            Gizmos.DrawLine(top1, left1);
            Gizmos.DrawLine(left1, bottom1);
            Gizmos.DrawLine(bottom1, right1);
            Gizmos.DrawLine(right1, top1);
        }
    }

}

