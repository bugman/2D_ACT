﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace TwlAction
{
    /// <summary>
    /// 需要展示的技能
    /// </summary>
    public class SkillDisplayWidget : BaseDisplayWidget
    {
        private BaseAction skillAgent;
        protected Vector3 pos = Vector3.zero;
        private SpriteRenderer render;
        public SpriteRenderer Render
        {
            get
            {
                return render;
            }
            set
            {
                this.render = value;
            }
        }

        public void setSkill(BaseAction skill)
        {
            skillAgent = skill;
        }

        protected override void onStart()
        {
            loadModel(skillAgent.resName);
        }

        protected override void loadFinish()
        {
            this.render = this.Prefab.GetComponent<SpriteRenderer>();

            pos.x = skillAgent.box3d.centerX;
            pos.y = skillAgent.box3d.centerY+skillAgent.box3d.MaxH;
            this.Trans.position = pos;
        }

        public override void updateRenderOrder()
        {
            if (this.Render != null)
            {
                float y = skillAgent.box3d.centerY;
                int order = y > 0 ? maxOrder - Mathf.CeilToInt(y * magnifyOrder) : maxOrder + Mathf.CeilToInt(y * magnifyOrder) * -1;
                this.Render.sortingOrder = order;

                pos.x = skillAgent.box3d.centerX;
                pos.y = skillAgent.box3d.centerY + skillAgent.box3d.MaxH;
                this.Trans.position = pos;
            }
        }

#if Show_Gizmos
        void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            //float h = skillAgent.box3d.MaxH;
            //Vector3 top = new Vector3(skillAgent.box3d.centerX - skillAgent.box3d.length / 2, skillAgent.box3d.centerY + skillAgent.box3d.width / 2+ h);
            //Vector3 bottom = new Vector3(skillAgent.box3d.centerX + skillAgent.box3d.length / 2, skillAgent.box3d.centerY + skillAgent.box3d.width / 2+ h);
            //Vector3 left = new Vector3(skillAgent.box3d.centerX - skillAgent.box3d.length / 2, skillAgent.box3d.centerY - skillAgent.box3d.width / 2+ h);
            //Vector3 right = new Vector3(skillAgent.box3d.centerX + skillAgent.box3d.length / 2, skillAgent.box3d.centerY - skillAgent.box3d.width / 2+ h);
            //Gizmos.DrawLine(bottom, top);
            //Gizmos.DrawLine(top, left);
            //Gizmos.DrawLine(left, right);
            //Gizmos.DrawLine(right, bottom);
            UnityEngine.Vector3 top1 = new UnityEngine.Vector3(skillAgent.box3d.Left, skillAgent.box3d.MaxH + skillAgent.box3d.centerY);
            UnityEngine.Vector3 bottom1 = new UnityEngine.Vector3(skillAgent.box3d.Right, skillAgent.box3d.MinH + skillAgent.box3d.centerY);
            UnityEngine.Vector3 left1 = new UnityEngine.Vector3(skillAgent.box3d.Left, skillAgent.box3d.MinH + skillAgent.box3d.centerY);
            UnityEngine.Vector3 right1 = new UnityEngine.Vector3(skillAgent.box3d.Right, skillAgent.box3d.MaxH + skillAgent.box3d.centerY);
            Gizmos.color = Color.green;
            Gizmos.DrawLine(top1, left1);
            Gizmos.DrawLine(left1, bottom1);
            Gizmos.DrawLine(bottom1, right1);
            Gizmos.DrawLine(right1, top1);
        }

#endif


    }

}