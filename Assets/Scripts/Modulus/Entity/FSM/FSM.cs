﻿using System;
using System.Collections.Generic;

/// <summary>
/// 状态机管理器
/// 每个状态的时间长度是具体的逻辑帧
/// 每次tick 走逻辑帧
/// 逻辑帧到达退出状态
/// todo
/// 
/// A攻击B
/// B站立{
///      1.普通攻击 后退
///      2.浮空攻击 浮空
/// }
/// B浮空{
///     1.普通攻击  浮空
///     2.浮空攻击  浮空
/// }
/// B倒地{
///     1.普通攻击  倒地
///     2.浮空攻击  轻微浮空
/// }
/// </summary>
public class FSM
{
    public FSM(BaseEntity agent)
    {
        this.agent = agent;
    }

    //enum做key 会有GC
    private Dictionary<int, BaseState> pool = new Dictionary<int, BaseState>();
    private BaseState currState = null;
    private BaseEntity agent;
    public FSM_Flag Flag
    {
        get
        {
            if (currState != null)
                return currState.Flag;
            return FSM_Flag.None;
        }
    }

    public virtual void tick()
    {
        if (currState != null)
        {
            currState.onTick();
        }
    }

    public virtual void onRender()
    {
        if (currState != null)
        {
            currState.onRender();
        }
    }

    /// <summary>
    /// 转换状态
    /// </summary>
    /// <param name="flag"></param>
    /// <param name="args"></param>
    public bool transFsm(FSM_Flag fsmFlag, FSMArgs args = null, bool forceTrans = false)
    {
        int flag = (int)fsmFlag;
        bool isSuccess = false;
        if (!forceTrans && currState != null && currState.Flag == fsmFlag)
        {
            currState.onRefresh(args);
            return true;
        }
        if (pool.ContainsKey(flag))
        {
            if (currState == null || forceTrans || currState.allow(fsmFlag))
            {
                if (currState != null)
                    currState.onExit();
                currState = pool[flag];
                currState.onEnter(args);
                isSuccess = true;
            }
        }
        return isSuccess;
    }

    /// <summary>
    /// 添加一个状态
    /// </summary>
    /// <param name="flag"></param>
    /// <param name="state"></param>
    public void addState(FSM_Flag fsmFlag, BaseState state)
    {
        int flag = (int)fsmFlag;
        state.Flag = fsmFlag;
        state.Agent = this.agent;
        if (pool.ContainsKey(flag))
        {
            pool.Remove(flag);
        }
        pool.Add(flag, state);
    }
}

