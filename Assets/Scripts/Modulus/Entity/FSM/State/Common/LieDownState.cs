﻿using System;
using System.Collections.Generic;
using TwlPhy;

/// <summary>
/// 倒地平躺2帧
/// </summary>
public class LieDownState : BaseState
{
    protected int enterFrame = 0;
    protected int lieFrame = 2;

    public override void onEnter(FSMArgs args = null)
    {
        enterFrame = FrameMgr.NowFrame;
    }

    public override void onTick()
    {
        if (FrameMgr.NowFrame - enterFrame > lieFrame)
        {
            Agent.transFsm(FSM_Flag.Idle, null, true);
        }
    }

}

