﻿using System;
using System.Collections.Generic;
using TwlPhy;

/// <summary>
/// 实体被击(参考DNF)
/// 1:站立受击 
/// ｛
///    受击类型: 自适应 
///    
/// ｝击退 则击退 浮空则浮空
/// 2:浮空受击 受击类型 
/// </summary>
public class HitState : BaseState
{
    protected HitConfig hitCfg;
    protected int enterFrame = 0;
    protected Vector3 casterPos = Vector3.zero;

    public override void onEnter(FSMArgs args = null)
    {
        executeArgs(args);
    }

    public override void onRefresh(FSMArgs args = null)
    {
        executeArgs(args);
    }

    protected void executeArgs(FSMArgs args)
    {
        enterFrame = FrameMgr.NowFrame;
        casterPos = (Vector3)args.data;
        hitCfg = SkillConfigHelper.Instance.getHitCfg(args.cfgId);
        if (hitCfg != null)
        {
            enterFrame = FrameMgr.NowFrame;
            playAnim();
            playMove();
        }
        else
        {
            Logger.logError("读取配置失败HitConfig  " + args.cfgId);
        }
    }

    protected virtual void playAnim()
    {
        Agent.playAnim(hitCfg.hitAnim, false, "idle", true);
    }

    protected float hitBackDis = 0.25f;
    protected virtual void playMove()
    {
        Vector2 dir = casterPos.x * 0.001f < Agent.DyBox.centerX ? Vector2.right : Vector2.left;
        float dis = 0;
        bool isKnock = Physics2D.moveCast(Agent.DyBox, dir, hitBackDis, ref dis);
        Agent.doMove(dir * dis);
    }

    public override void onTick()
    {
        if (FrameMgr.NowFrame - enterFrame > hitCfg.frameNum)
        {
            Agent.transFsm(FSM_Flag.Idle, null, true);
        }
    }

    //击退 todo

}

