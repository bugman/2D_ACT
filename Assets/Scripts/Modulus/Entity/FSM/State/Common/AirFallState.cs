﻿using System;
using System.Collections.Generic;
using TwlPhy;

/// <summary>
/// 受击掉落只与玩家当前高度有关
/// </summary>
public class AirFallState : BaseState
{
    protected float enterHeight = 0f;//进入时高度 用于判断是否弹起
    protected int enterFrame = 0;

    public override void onEnter(FSMArgs args = null)
    {
        enterFrame = FrameMgr.NowFrame;
        enterHeight = Agent.DyBox.centerZ;
    }

    protected virtual void fallMove()
    {
        float add = (FrameMgr.NowFrame - enterFrame) * speedDamping + minFlySpeed;
        add = Agent.DyBox.centerZ <= add ? Agent.DyBox.centerZ : add;
        Agent.updatePosHeight(Agent.DyBox.centerZ - add);
        //Agent.doMove(Vector2.down * add);
    }

    //下落判断高度
    public override void onTick()
    {
        fallMove();
        if (Agent.DyBox.centerZ <= 0)
        {
            //到达 根据当前高度判断是否应该再弹起
            if (enterHeight >= reboundHeight)
            {
                int force = Mathf.FloorToInt(enterHeight / reboundForce);
                Logger.log("enterHeight " + enterHeight + "   reboundForce " + reboundForce + "  force  " + force);
                FSMArgs args = new FSMArgs();
                args.cfgId = force;
                Agent.transFsm(FSM_Flag.AirHit, args, true);
            }
            else {
                Agent.transFsm(FSM_Flag.LineDown, null, true);
            }
        }
    }

}

