﻿//using System;
//using System.Collections.Generic;
//using TwlPhy;
//using TwlAction;

///// <summary>
///// 技能状态基类
///// </summary>
//public class SkillState : BaseState
//{
//    protected int enterFrame = 0;
//    protected SkillConfig skillCfg;

//    public override void onEnter(FSMArgs args = null)
//    {
//        enterFrame = FrameMgr.NowFrame;
//        skillCfg = args.data as SkillConfig;
//        int index = FrameMgr.NowFrame - enterFrame + 1;
//        checkEvent(index);
//    }

//    public override void onRefresh(FSMArgs args = null)
//    {

//    }

//    public override void onTick()
//    {
//        int index = FrameMgr.NowFrame - enterFrame + 1;
//        checkEvent(index);
//        if (index >= skillCfg.frameNum)
//        {
//            this.Agent.transFsm(FSM_Flag.Idle, null, true);
//        }
//    }

//    //主角计算伤害盒
//    //网络玩家做展示 动画 特效 声音 位移...
//    protected virtual void checkEvent(int index)
//    {
//        //Logger.log("<color=red>checkEvent index: </color>" + index);
//        //如果动画表有这一帧信息 可能是loop动画 还是应该填写动画配置
//        if (skillCfg.animFrame.ContainsKey(index))
//        {
//            this.Agent.playAnim(skillCfg.animFrame[index]);
//        }
//        //播放特效
//        if (skillCfg.effectFrame.ContainsKey(index))
//        {
//            //todo
//        }
//        //播放音效
//        if (skillCfg.audioFrame.ContainsKey(index))
//        {
//            //todo
//        }
//        //位移
//        if (skillCfg.moveFrame.ContainsKey(index))
//        {
//            //todo
//        }
//        //子弹
//        if (skillCfg.bulletFrame.ContainsKey(index))
//        {
//            SkillData skillData = new SkillData();
//            skillData.size = new Vector3(0.3f, 0.1f, 0.1f);
//            skillData.ownerId = Agent.UID;
//            skillData.skillGuid = MathUtils.UniqueID;
//            skillData.skillId = 1001;
//            skillData.skillType = E_Skill_Type.Bullet;
//            SkillMgr.Instance.create<BulletSkill>(skillData);
//             Config.AttackConfig.get(1001);
//        }
//        //伤害检测 主角重写
//    }
//}

