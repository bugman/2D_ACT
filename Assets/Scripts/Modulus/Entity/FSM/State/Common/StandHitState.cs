﻿using System;
using System.Collections.Generic;
using TwlPhy;
using Config;

/// <summary>
/// 站立击退 带僵直时间
/// 退出条件 持续帧完毕(移动可能是20帧 持续可能是30帧)
/// A受击 僵直10帧  位移5帧 每帧0.2f位移
/// 3帧之后 受击 僵直20帧 位移1帧 每帧0.4f位移
/// 直接用新的位移参数 最高的僵直参数
/// </summary>
public class StandHitState : BaseState
{
    protected int liveFrame = 0;
    protected int moveFrame = 0;
    protected int enterFrame = 0;
    protected Vector3 casterPos = Vector3.zero;
    protected float hitBackDis = 0.25f;

    public override void onEnter(FSMArgs args = null)
    {
        executeArgs(args);
    }

    public override void onRefresh(FSMArgs args = null)
    {
        executeArgs(args);
    }

    protected void executeArgs(FSMArgs args)
    {
        enterFrame = FrameMgr.NowFrame;
        casterPos = (Vector3)args.data;
        MoveConfig moveCfg = MoveConfig.get(args.cfgId);
        if (moveCfg != null)
        {
            hitBackDis = moveCfg.forceSpeed;
            moveFrame = moveCfg.force;
            if (moveCfg.liveFrame > liveFrame) {
                liveFrame = moveCfg.liveFrame;
            }
        }
        else
        {
            Logger.logError("未找到配置moveCfg  " + args.cfgId);
            forceToIdle();
        }
    }

    protected virtual void playMove()
    {
        Vector2 dir = casterPos.x * 0.001f < Agent.DyBox.centerX ? Vector2.right : Vector2.left;
        float dis = 0;
        bool isKnock = Physics2D.moveCast(Agent.DyBox, dir, hitBackDis, ref dis);
        Agent.doMove(dir * dis);
    }

    public override void onTick()
    {
        if (moveFrame>0)
        {
            playMove();
        }
        moveFrame--;        
        if (liveFrame<=0)
        {
            Agent.transFsm(FSM_Flag.Idle, null, true);
        }
        liveFrame--;
    }
}

