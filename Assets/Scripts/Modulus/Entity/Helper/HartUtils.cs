﻿using Pool;
using TwlPhy;
using System.Collections.Generic;

/// <summary>
/// 伤害包处理
/// </summary>
public class HartUtils
{
    /// <summary>
    /// 伤害盒检测
    /// </summary>
    /// <param name="caster"></param>
    /// <param name="hitId"></param>
    /// <returns></returns>
    public static List<long> checkHit(long caster, int hitId)
    {
        return checkHit(EntityMgr.getRole<BaseEntity>(caster), hitId);
    }

    public static List<long> checkHit(long caster, DymicBox3D box)
    {
        return checkHit(EntityMgr.getRole<BaseEntity>(caster), box);
    }

    public static List<long> checkHit(BaseEntity caster, int hitId)
    {
        HitConfig cfg = SkillConfigHelper.Instance.getHitCfg(hitId);
        float x = caster.DyBox.centerX + (caster.lookFlag == LookFlag.Right ? cfg.offsetX : cfg.offsetX * -1);
        float y = caster.DyBox.centerY + cfg.offsetY;
        float z = caster.DyBox.centerZ;
        DymicBox3D box = new DymicBox3D(x, y, z, cfg.sizeX, cfg.sizeY, cfg.sizeH);
        return checkHit(caster, box);
    }

    public static List<long> checkHit(BaseEntity caster, DymicBox3D box)
    {
        List<long> hiters = new List<long>();
        if (caster == null) return hiters;
        var ier = EntityMgr.getPool().GetEnumerator();
        long uid = caster.UID;
        #if Show_Gizmos
        GizmosHelper.drawBox(box);
        #endif
        while (ier.MoveNext())
        {
            if (ier.Current.Value.UID != uid)
            {
                if (Physics3D.isCollision(box, ier.Current.Value.HitBox))
                {
                    hiters.Add(ier.Current.Value.UID);
                }
            }
        }
        return hiters;
    }
}