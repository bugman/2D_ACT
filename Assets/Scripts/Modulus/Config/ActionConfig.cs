using System.Collections.Generic;
using ConfigMap;
using Config;
namespace Config
{
public class ActionConfig
{
   public int  id;
   public string  name;
   public int  maxFrame;
   public int  lockLevel;
   public Dictionary<int,int>  damageFrame;
   public Dictionary<int,int>  moveFrame;
   public Dictionary<int,int>  effectFrame;
   public Dictionary<int,int>  audioFrame;
   public Dictionary<int,int>  animFrame;

   public ActionConfig (int id,string name,int maxFrame,int lockLevel,Dictionary<int,int> damageFrame,Dictionary<int,int> moveFrame,Dictionary<int,int> effectFrame,Dictionary<int,int> audioFrame,Dictionary<int,int> animFrame){
     this.id = id;
     this.name = name;
     this.maxFrame = maxFrame;
     this.lockLevel = lockLevel;
     this.damageFrame = damageFrame;
     this.moveFrame = moveFrame;
     this.effectFrame = effectFrame;
     this.audioFrame = audioFrame;
     this.animFrame = animFrame;

   }
   public static ActionConfig get(int key)
   {
      return ActionConfigMap.Instance.get(key);
   }
   public static Dictionary<int, ActionConfig> getAll()
   {
      return ActionConfigMap.Instance.getAll();
   }
   }
}
namespace ConfigMap
{
public class ActionConfigMap: Singleton<ActionConfigMap>
{
   public Dictionary<int, ActionConfig> map = null; 
   protected override void initialize() 
   {
      map = new Dictionary<int, ActionConfig>(); 
      map.Add(100001,new ActionConfig(100001,"枪手普攻",10,1,new Dictionary<int, int>() { {3,1001},},null,null,null,new Dictionary<int, int>() { {1,1001},}));
      map.Add(100002,new ActionConfig(100002,"枪手浮空弹",10,1,new Dictionary<int, int>() { {3,1003},},null,null,null,new Dictionary<int, int>() { {1,1001},}));
      map.Add(500001,new ActionConfig(500001,"受击",10,2,null,new Dictionary<int, int>() { {1,1001},},null,null,new Dictionary<int, int>() { {1,1004},}));
      map.Add(500002,new ActionConfig(500002,"受击",10,2,null,new Dictionary<int, int>() { {1,1004},},null,null,null));
}
   public ActionConfig get(int key)
   {
      ActionConfig cfg;
     if (map.TryGetValue(key, out cfg))
   {
        return cfg;
   }
   return null;
   }
   public  Dictionary<int, ActionConfig> getAll()
   {
      return map;
   }
}
}
