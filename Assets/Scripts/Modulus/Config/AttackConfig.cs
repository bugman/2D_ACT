using System.Collections.Generic;
using ConfigMap;
using Config;
namespace Config
{
public class AttackConfig
{
   public int  id;
   public string  name;
   public int  attckType;
   public int  cfgId;
   public float[]  offset;
   public float[]  boxSize;

   public AttackConfig (int id,string name,int attckType,int cfgId,float[] offset,float[] boxSize){
     this.id = id;
     this.name = name;
     this.attckType = attckType;
     this.cfgId = cfgId;
     this.offset = offset;
     this.boxSize = boxSize;

   }
   public static AttackConfig get(int key)
   {
      return AttackConfigMap.Instance.get(key);
   }
   public static Dictionary<int, AttackConfig> getAll()
   {
      return AttackConfigMap.Instance.getAll();
   }
   }
}
namespace ConfigMap
{
public class AttackConfigMap: Singleton<AttackConfigMap>
{
   public Dictionary<int, AttackConfig> map = null; 
   protected override void initialize() 
   {
      map = new Dictionary<int, AttackConfig>(); 
      map.Add(1001,new AttackConfig(1001,"普通子弹",2,1001,null,null));
      map.Add(1002,new AttackConfig(1002,"",0,0,null,null));
      map.Add(1003,new AttackConfig(1003,"浮空弹",2,1002,null,null));
}
   public AttackConfig get(int key)
   {
      AttackConfig cfg;
     if (map.TryGetValue(key, out cfg))
   {
        return cfg;
   }
   return null;
   }
   public  Dictionary<int, AttackConfig> getAll()
   {
      return map;
   }
}
}
