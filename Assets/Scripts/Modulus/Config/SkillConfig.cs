﻿using System;
using System.Collections.Generic;

//测试提供配置 后面都用Lua配置
/// <summary>
/// 动画帧:动画名    可能多个
/// 特效帧:特效Id    可能多个
/// 伤害检测帧:伤害检测Id  多个
/// 位移帧:位移id 可能多个
/// </summary>
public class SkillConfig
{
    public int skillId;//技能Id
    public int frameNum;//技能持续帧
    int[] keyFrame;//1帧播放特效 1帧播放音效 1帧检测伤害
    int[] keyFramePlayerType;//对应上面的类型
    int[] keyFramePlayerCfgId;//对应上面的配置id


    public Dictionary<int, int> hitBoxFrame;//伤害盒
    public Dictionary<int, string> animFrame;//动画
    public Dictionary<int, string> audioFrame;//音效
    public Dictionary<int, int> effectFrame;//特效
    public Dictionary<int, int> moveFrame;//位移
    public Dictionary<int, int> bulletFrame;//子弹
}

//测试一下伤害检测
public class HitConfig
{
    public int id;//hit id 
    public int frameNum;//持续帧
    //盒子X Y H
    public float sizeX;
    public float sizeY;
    public float sizeH;
    //盒子偏移 X Y H 
    public float offsetX;//玩家前方偏移
    public float offsetY;//Y轴偏移
    //public float offsetH;//
    //受击动画
    public string hitAnim;
    //造成的位移
    public int moveId;
}

public class SkillConfigHelper : Singleton<SkillConfigHelper>
{
    private Dictionary<int, SkillConfig> skillCfgs = new Dictionary<int, SkillConfig>();
    private Dictionary<int, HitConfig> hitCfgs = new Dictionary<int, HitConfig>();

    protected override void initialize()
    {
        HitConfig hitCfg = new HitConfig();
        hitCfg.sizeX = 3;
        hitCfg.sizeY = 2;
        hitCfg.sizeH = 2;
        hitCfg.offsetX = 1;
        hitCfg.offsetY = 1;
        hitCfg.hitAnim = "hit old";
        hitCfg.frameNum = 30;
        hitCfgs.Add(1001, hitCfg);

        HitConfig hitCfg2 = new HitConfig();
        hitCfg2.sizeX = 3;
        hitCfg2.sizeY = 2;
        hitCfg2.sizeH = 2;
        hitCfg2.offsetX = 1;
        hitCfg2.offsetY = 1;
        hitCfg2.hitAnim = "hit";
        hitCfg2.frameNum = 50;
        hitCfgs.Add(1002, hitCfg2);

        SkillConfig cfg = new SkillConfig();
        cfg.skillId = 1001;
        cfg.frameNum = 100;

        cfg.animFrame = new Dictionary<int, string>();
        cfg.animFrame.Add(1, "gun toss");
        cfg.effectFrame = new Dictionary<int, int>();
        cfg.audioFrame = new Dictionary<int, string>();
        cfg.moveFrame = new Dictionary<int, int>();
        cfg.hitBoxFrame = new Dictionary<int, int>();
        cfg.bulletFrame = new Dictionary<int, int>();
        cfg.hitBoxFrame.Add(30, 1001);
        cfg.hitBoxFrame.Add(45, 1001);
        cfg.hitBoxFrame.Add(60, 1001);
        cfg.hitBoxFrame.Add(75, 1001);
        cfg.hitBoxFrame.Add(90, 1001);

        SkillConfig cfg2 = new SkillConfig();
        cfg2.skillId = 1002;
        cfg2.frameNum = 10;

        cfg2.animFrame = new Dictionary<int, string>();
        cfg2.animFrame.Add(1, "shoot");
        cfg2.effectFrame = new Dictionary<int, int>();
        cfg2.audioFrame = new Dictionary<int, string>();
        cfg2.moveFrame = new Dictionary<int, int>();
        cfg2.hitBoxFrame = new Dictionary<int, int>();
        cfg2.bulletFrame = new Dictionary<int, int>();
        cfg2.bulletFrame.Add(2, 1001);

        skillCfgs.Add(cfg.skillId, cfg);
        skillCfgs.Add(cfg2.skillId, cfg2);
    }


    public SkillConfig getConfig(int id)
    {
        if (skillCfgs.ContainsKey(id))
            return skillCfgs[id];
        return null;
    }

    public HitConfig getHitCfg(int id)
    {
        if (hitCfgs.ContainsKey(id))
            return hitCfgs[id];
        return null;
    }

}

