using System.Collections.Generic;
using ConfigMap;
using Config;
namespace Config
{
public class AnimConfig
{
   public int  id;
   public string  animName;
   public bool  isLoop;
   public string  backAnim;

   public AnimConfig (int id,string animName,bool isLoop,string backAnim){
     this.id = id;
     this.animName = animName;
     this.isLoop = isLoop;
     this.backAnim = backAnim;

   }
   public static AnimConfig get(int key)
   {
      return AnimConfigMap.Instance.get(key);
   }
   public static Dictionary<int, AnimConfig> getAll()
   {
      return AnimConfigMap.Instance.getAll();
   }
   }
}
namespace ConfigMap
{
public class AnimConfigMap: Singleton<AnimConfigMap>
{
   public Dictionary<int, AnimConfig> map = null; 
   protected override void initialize() 
   {
      map = new Dictionary<int, AnimConfig>(); 
      map.Add(1001,new AnimConfig(1001,"shoot",false,"idle"));
      map.Add(1002,new AnimConfig(1002,"gun toss",false,"idle"));
      map.Add(1003,new AnimConfig(1003,"hit",false,""));
      map.Add(1004,new AnimConfig(1004,"hit old",false,""));
}
   public AnimConfig get(int key)
   {
      AnimConfig cfg;
     if (map.TryGetValue(key, out cfg))
   {
        return cfg;
   }
   return null;
   }
   public  Dictionary<int, AnimConfig> getAll()
   {
      return map;
   }
}
}
