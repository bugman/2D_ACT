﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace ActTools
{
    /// <summary>
    /// 技能编辑器
    /// </summary>
    public class ActTool : EditorWindow
    {
        private static ActTool _instance = null;
        private bool isInit = false;

        [MenuItem("ToolsWindow/ActTool")]
        public static void showWindow()
        {
            if (_instance == null)
            {
                _instance = (ActTool)EditorWindow.GetWindow(typeof(ActTool), false, "ActTool", true);
                _instance.maxSize = new Vector2(1280, 720);
                GUIContent content = new GUIContent();
                content.text = "ActTool";

                _instance.titleContent = content;
                _instance.isInit = true;
            }
        }

        private void OnGUI()
        {
            EditorGUILayout.BeginVertical();
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("加载模型", GUILayout.Width(160), GUILayout.Height(40))) {

            }
            GUILayout.Space(10);
            if (GUILayout.Button("加载动画", GUILayout.Width(160), GUILayout.Height(40)))
            {

            }
            EditorGUILayout.EndHorizontal();
            //GUILayout

            EditorGUILayout.EndVertical();
        }




    }

    

}


